# Setup git if not have account

* git config --global user.name "GitName"
* git config --global user.email "GitEmail"
* ssh-keygen -t rsa -C "GitEmail"

#Setup:

1. Install [docker](https://docs.docker.com/engine/installation/) and [docker-compose](https://docs.docker.com/compose/). 
Complete [post-install#manage-docker-as-a-non-root-user](https://docs.docker.com/engine/installation/linux/linux-postinstall/#/manage-docker-as-a-non-root-user)
instructions.
2. Verify that you have correct docker and docker-compose versions.
```docker -v``` should output ```Docker version 1.13.0, build 49bf474```.
3. Build: ```docker build -t essprendimai_fluentd https://Ernestyno@bitbucket.org/essprendimai/docker-fluentd.git```
4. Run: ```docker run -p 8888:8888 essprendimai_fluentd```
5. Or run: ```docker-compose up```

#Configuratin include:

- json data store from http to log file
- json data store from http to mysql database
- json data store from http via gmail

#Json data store from http to log file example

- Run docker: ```docker run -p 8888:8888 essprendimai_fluentd```
- Open terminal and send data with curl: ```curl -X POST -d 'json={"test":"data"}' http://localhost:8888/test.tag;```
- Exec docker container to see data in log:
-- ```docker ps```
-- See: 87fd0188b772 | essprendimai_fluentd ...
-- Look log file name: ```docker exec 87fd0188b772 ls /var/log/fluent/test-app```
-- See: buffer.b55d7f882b8892dc3834fee92067bcc03.log
-- Watch file data: ```docker exec 87fd0188b772 cat /var/log/fluent/test-app/buffer.b55d7f882b8892dc3834fee92067bcc03.log```

#Json data store from http to mysql database

- Also build mysql: ```docker build -t essprendimai_mariadb https://Ernestyno@bitbucket.org/essprendimai/docker-mariadb.git```
- Run mysql with name (name used at conf/fluent.conf): ```docker run -d -p 3306:3306 --name essprendimai_mariadb_container essprendimai_mariadb```
- Connect to mysql and create table ```
CREATE TABLE users (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(30) NOT NULL,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
)
```
- Run fluentd and link mariadb container: ```docker run -p 8888:8888 --name essprendimai_fluentd_container --link essprendimai_mariadb_container essprendimai_fluentd```
- Open terminal and send data with curl: ```curl -X POST -d 'json={"name":"data"}' http://localhost:8888/mysql.input;```
- After 10s should be data in database somethink like: 
id  name	created_at			updated_at
1	data	2017-11-09 00:49:24	2017-11-09 00:49:24
- Witch field to write on send json tag writed at fluent.conf <match mysql.input> ...


#Json data store from http via gmail

- Also build mysql: ```docker build -t essprendimai_mariadb https://Ernestyno@bitbucket.org/essprendimai/docker-mariadb.git```
- Run mysql with name (name used at conf/fluent.conf): ```docker run -d -p 3306:3306 --name essprendimai_mariadb_container essprendimai_mariadb```
- At conf/fluent.conf -> change gmail settings
- Run fluentd and link mariadb container: ```docker run -p 8888:8888 --name essprendimai_fluentd_container --link essprendimai_mariadb_container essprendimai_fluentd```
- Open terminal and send data with curl: ```curl -X POST -d 'json={"foo":"bar","message":"hello"}' http://localhost:8888/mail.test;```
- And you get mail to mail.