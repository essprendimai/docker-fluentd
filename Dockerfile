FROM ruby:2.2.3

RUN gem install fluentd oj

ADD ./conf/fluent.conf /etc/fluent/fluent.conf

RUN gem install fluent-plugin-mysql
RUN gem install fluent-plugin-mail

EXPOSE 8888

CMD ["fluentd"]
